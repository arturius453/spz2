using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace lb2
{
    class MainWindow : Window
    {
        [UI] private ListStore _liststore = null;
        [UI] private Box _controlBox = null;
        [UI] private TreeView _treeView= null;

        public MainWindow() : this(new Builder("MainWindow.glade")) { }

        private MainWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            builder.Autoconnect(this);
            DeleteEvent += Window_DeleteEvent;
	   ( _controlBox.Children[0] as Button).Clicked+=AddB_Clicked;
	   ( _controlBox.Children[1] as Button).Clicked+=CpB_Clicked;
	   ( _controlBox.Children[2] as Button).Clicked+=HireB_Clicked;
	   ( _controlBox.Children[3] as Button).Clicked+=CompB_Clicked;
	   ( _controlBox.Children[4] as Button).Clicked+=MergeB_Clicked;
	   ( _controlBox.Children[5] as Button).Clicked+=InvB_Clicked;
        }
	public Factory getFfromRow(int id){
		TreeIter iter;
		//_liststore.GetIter(out iter,id);
		_liststore.IterNthChild(out iter,id);
		int [] fiedls=new int[7];
		string nm=_liststore.GetValue(iter,0) as string;
		for(int j=0;j<7;j++)
			fiedls[j]=(int)_liststore.GetValue(iter,j+1);
		return new Factory(nm,fiedls as int[]) ;
	}
	
        private void AddB_Clicked(object sender, EventArgs a)
        {
		(new AddDialog(this)).Run();
        }
        private void HireB_Clicked(object sender, EventArgs a)
        {
		if(_treeView.Selection.CountSelectedRows()>0)
			(new HireDialog(this)).Run();
        }
        private void InvB_Clicked(object sender, EventArgs a)
        {
		if(_treeView.Selection.CountSelectedRows()>0)
			(new InvDialog(this)).Run();
        }
        private void CpB_Clicked(object sender, EventArgs a)
        {
		foreach(var i in _treeView.Selection.GetSelectedRows()){
			TreeIter iter;
			_liststore.GetIter(out iter,i);
			//_liststore.IterNthChild(out iter,i.Indices[0]);
			object[] fiedls=new object[8];
			for(int j=0;j<8;j++)
				fiedls[j]=_liststore.GetValue(iter,j);
			fiedls[0]+="new";
			_liststore.InsertWithValues(i.Indices[0],fiedls);
		}
        }

        private void CompB_Clicked(object sender, EventArgs a)
        {
		var md= new MessageDialog(this,
		DialogFlags.DestroyWithParent | DialogFlags.Modal |DialogFlags.UseHeaderBar
		, MessageType.Info,ButtonsType.Ok,"ЗаглушкаТекст" );
		if(_treeView.Selection.CountSelectedRows()!=2){
			md.Title="Помилка";
			md.Text="Треба вибрати лише 2 заводи";
		}
		else {
			Factory []f=new Factory[2];
			int k=0;
			foreach(var i in _treeView.Selection.GetSelectedRows()){
				 f[k]=getFfromRow(i.Indices[0]);k++;
			}
			if (f[0].CompareTo(f[1])==0) md.Text="Заводи рівні";
			else 
			if (f[0].CompareTo(f[1])>0) md.Text=$"{f[0].name} більше {f[1].name}";
			else  md.Text=$"{f[0].name} менше {f[1].name}";
			md.Title="Порівняння";
		}
		md.Run();	md.Destroy();
		return;
        }

        private void MergeB_Clicked(object sender, EventArgs a)
        {
		if(_treeView.Selection.CountSelectedRows()!=2){
			var md= new MessageDialog(this,
			DialogFlags.DestroyWithParent | DialogFlags.Modal |DialogFlags.UseHeaderBar
			, MessageType.Error,ButtonsType.Ok,"Треба вибрати лише 2 заводи" );
			md.Title="Помилка";
			md.Run();	md.Destroy();
		}
		else {
			Factory []f=new Factory[2];
			int k=0;
			foreach(var i in _treeView.Selection.GetSelectedRows()){
				 f[k]=getFfromRow(i.Indices[0]);k++;
			}
			TreeIter iter;
 			TreePath[] treePath = _treeView.Selection.GetSelectedRows();
			for (int i  = treePath.Length; i > 0; i--)
			{
			    _liststore.GetIter(out iter, treePath[(i - 1)]);
			    _liststore.Remove(ref iter);
			}
			Factory nf=f[0]+f[1];
			_liststore.AppendValues(nf.name,nf.amountOfWorkshops,nf.amountOfWorkers,
			nf.amountOfMasters,nf.workerSalary,nf.masterSalary,
			nf.monthlyProfitPerWorker,nf.monthlyProfitPerMaster);

		}
		return;
        }
    	private	class AddDialog : Dialog
	{	
		MainWindow parent;
        	[UI] private Box _dlgBox = null;
        	[UI] private Button _applyButton = null;
	        public AddDialog(MainWindow pt) : this(pt,new Builder("MainWindow.glade")) {
			}
	
	        private AddDialog(MainWindow pt,Builder builder) : base(builder.GetRawOwnedObject("_addDialog"))
	        {
			parent=pt; 
		        builder.Autoconnect(this);
			_applyButton.Clicked+=Aplly_clicked;
			((_applyButton.Parent as ButtonBox).Children[1] 
			 as Button).Clicked+=delegate{	Destroy();};
		}
        	private void Aplly_clicked(object sender, EventArgs a){
			string nm=(((_dlgBox.Children[0] as Box).Children[1]) as Entry).Text;
			
			int[] fiedls=new int [7];
			for(int i=0;i<7;i++)
		       		fiedls[i]=(((_dlgBox.Children[i+1] as Box).Children[1]) as SpinButton).ValueAsInt;
			try {
				new Factory(nm,fiedls);
			}
			catch(Exception e){
				var md= new MessageDialog(this,
		                DialogFlags.DestroyWithParent | DialogFlags.Modal |DialogFlags.UseHeaderBar
				, MessageType.Error,ButtonsType.Ok, e.Message);
				md.Title="Помилка";	md.Run();	md.Destroy();
				return;
			}
			parent._liststore.AppendValues(nm,fiedls[0],fiedls[1],fiedls[2],fiedls[3],fiedls[4],fiedls[5],fiedls[6]);
			Destroy();
		}
	}
   	private	class HireDialog : Dialog
	{	
		MainWindow parent;
        	[UI] private Box _dlgBox1 = null;
        	[UI] private Button _applyButton1 = null;
	        public HireDialog(MainWindow pt) : this(pt,new Builder("MainWindow.glade")) {
			}
	
	        private HireDialog(MainWindow pt,Builder builder) : base(builder.GetRawOwnedObject("HireDialog"))
	        {
			parent=pt; 
		        builder.Autoconnect(this);
			_applyButton1.Clicked+=Aplly_clicked;
			((_applyButton1.Parent as ButtonBox).Children[1] 
			 as Button).Clicked+=delegate{	Destroy();};
		}
        	private void Aplly_clicked(object sender, EventArgs a){
			foreach(var i in parent._treeView.Selection.GetSelectedRows()){
			Factory f=parent.getFfromRow(i.Indices[0]);
			int choosen=(_dlgBox1.Children[0] as ComboBoxText).Active;
			try {
				if(choosen==0)//hire workers
					f.hireWorker(((_dlgBox1.Children[1] as Box).Children[1] 
								as SpinButton).ValueAsInt);
				else if (choosen==1)//hire masters
					f.hireMaster(((_dlgBox1.Children[1] as Box).Children[1] 
								as SpinButton).ValueAsInt);
			}
			catch(Exception e){
				var md= new MessageDialog(this,
		                DialogFlags.DestroyWithParent | DialogFlags.Modal |DialogFlags.UseHeaderBar
				, MessageType.Error,ButtonsType.Ok, e.Message);
				md.Title="Помилка";	md.Run();	md.Destroy();
				return;
			}
			TreeIter iter;
			parent._liststore.GetIter(out iter,i);
			if(choosen==0)//hire workers
				parent._liststore.SetValue(iter,2,f.amountOfWorkers);
			else if (choosen==1)//hire masters
				parent._liststore.SetValue(iter,3,f.amountOfMasters);
			}
			Destroy();
		}
	}
   	private	class InvDialog : Dialog
	{	
		MainWindow parent;
        	[UI] private SpinButton _investThing = null;
        	[UI] private Button _applyButton2 = null;
	        public InvDialog(MainWindow pt) : this(pt,new Builder("MainWindow.glade")) {
			}
	
	        private InvDialog(MainWindow pt,Builder builder) : base(builder.GetRawOwnedObject("InvDlg"))
	        {
			parent=pt; 
		        builder.Autoconnect(this);
			_applyButton2.Clicked+=Aplly_clicked;
			((_applyButton2.Parent as ButtonBox).Children[1] 
			 as Button).Clicked+=delegate{	Destroy();};
		}
        	private void Aplly_clicked(object sender, EventArgs a){
			foreach(var i in parent._treeView.Selection.GetSelectedRows()){
			Factory f=parent.getFfromRow(i.Indices[0]);
			int res=f.CalculateFutureIncome(_investThing.ValueAsInt);
			var md= new MessageDialog(this,
		               DialogFlags.DestroyWithParent | DialogFlags.Modal |DialogFlags.UseHeaderBar
				, MessageType.Info,ButtonsType.Ok, res.ToString());
			md.Title="Результат інвестицій:";	md.Run();	md.Destroy();
			}
			Destroy();
		}
	}
        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }
    }
}
