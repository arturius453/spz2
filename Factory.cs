﻿using System;

namespace lb2
{
    public struct Factory: IComparable,ICloneable
    {
        public string name               {get;set;}
        public int amountOfWorkshops     {get;set;}
        public int workerSalary          {get;set;}
        public int masterSalary          {get;set;}
        public int monthlyProfitPerWorker{get;set;}
        public int monthlyProfitPerMaster{get;set;}

        private int _amountOfWorkers;
        public int amountOfWorkers  {get=>_amountOfWorkers;
		set{
		if(value>10*amountOfMasters) 
			throw new ArgumentException("Not enough masters for setted amount of workers");
		if(value<0) 
			throw new ArgumentException("Negative amount of workers");
		_amountOfWorkers=value;
		}
	}
        private int _amountOfMasters;
        public int amountOfMasters  {get=>_amountOfMasters;
		set{
		if(10*value<_amountOfWorkers) 
			throw new ArgumentException("Too much workers for setted amount of masters");
		if(value<0) 
			throw new ArgumentException("Negative amount of masters");
		_amountOfMasters=value;
		}
	}
        public void hireWorker(int amount)
        {
            amountOfWorkers += amount;
        }
        public void hireMaster(int amount)
        {
            amountOfMasters += amount;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            if (obj is Factory otherFactory)
                return this.amountOfWorkshops.CompareTo(otherFactory.amountOfWorkshops);
            else
                throw new ArgumentException("Object is not a Factory");
        }
	public override string ToString()=>$"{name}";
	public object Clone() => MemberwiseClone();

        public Factory(string nm,int aOws,int aOw,int aOm,int WS,int MS,int PpW,int PpM){
		name=nm;amountOfWorkshops=aOws;
		workerSalary=WS;masterSalary=MS;
		monthlyProfitPerWorker=PpW;monthlyProfitPerMaster=PpM;
		_amountOfMasters=-1;
		_amountOfWorkers=-1;
		amountOfMasters=aOm;
		amountOfWorkers=aOw;
	}

        public Factory(string nm,int [] fields):this(nm,
			fields[0],
			fields[1],
			fields[2],
			fields[3],
			fields[4],
			fields[5],
			fields[6])   {}

        public Factory(Factory previousFactory)
        {
           // name = previousFactory.name;
           // amountOfWorkshops = previousFactory.amountOfWorkshops;
           // amountOfWorkers = previousFactory.amountOfWorkers;
           // amountOfMasters = previousFactory.amountOfMasters;
           // workerSalary = previousFactory.workerSalary;
           // masterSalary = previousFactory.masterSalary;
           // monthlyProfitPerWorker = previousFactory.monthlyProfitPerWorker;
           // monthlyProfitPerMaster = previousFactory.monthlyProfitPerMaster;
		this=(Factory) previousFactory.Clone();
        }

        public static Factory operator+(Factory a,Factory b)
        {
            Factory newFactory = new Factory();
            newFactory.amountOfMasters = a.amountOfMasters + b.amountOfMasters;
            newFactory.amountOfWorkers = a.amountOfWorkers + b.amountOfWorkers;
            newFactory.amountOfWorkshops = a.amountOfWorkshops + b.amountOfWorkshops;
            newFactory.name = a.name + " and " + b.name;
            newFactory.masterSalary = Math.Max(a.masterSalary, b.masterSalary);
            newFactory.workerSalary = Math.Min(a.workerSalary, b.workerSalary);
            newFactory.monthlyProfitPerMaster = Math.Max(a.monthlyProfitPerMaster , b.monthlyProfitPerMaster );
            newFactory.monthlyProfitPerWorker = Math.Max(a.monthlyProfitPerWorker , b.monthlyProfitPerWorker );

            return newFactory;
        }
	public int[] getIntFields(){
		int []flds=new int[7]{amountOfWorkshops,amountOfWorkers,
			amountOfMasters,workerSalary,masterSalary,
			monthlyProfitPerWorker,monthlyProfitPerMaster};
		return flds;
	}

    }

    public static class incomeExtension
    {
        public static int CalculateFutureIncome(this Factory factory,int invenstment)
        {
            int workersProfit = factory.monthlyProfitPerWorker * factory.amountOfWorkers;
            int mastersProfit = factory.monthlyProfitPerMaster * factory.amountOfWorkers;
            int someProfitFormula = (workersProfit + mastersProfit) * (invenstment / 10);
            return someProfitFormula;
        }
    }
}
